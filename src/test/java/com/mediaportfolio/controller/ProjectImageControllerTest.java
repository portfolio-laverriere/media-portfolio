package com.mediaportfolio.controller;

import com.mediaportfolio.exception.ImageConflictException;
import com.mediaportfolio.exception.ImageNotFoundException;
import com.mediaportfolio.model.ImageEntity;
import com.mediaportfolio.repository.ImageProjectRepository;
import com.mediaportfolio.repository.ImageRepository;
import com.mediaportfolio.service.dto.ImageDTO;
import com.mediaportfolio.service.mapper.ImageMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataIntegrityViolationException;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.List;

class ProjectImageControllerTest {

    @Mock
    ImageProjectRepository imageProjectRepository;

    @Mock
    ImageRepository imageRepository;

    @InjectMocks
    ProjectImageController projectImageController;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getAllImageForProject() {

        List<ImageDTO> imageMockList = new ArrayList<>();

        ImageDTO imageMock = ImageDTO.builder().id(1).name("Nom image").imagePath("Path image").build();
        imageMockList.add(imageMock);
        ImageDTO imageMock2 = ImageDTO.builder().id(2).name("Nom image 2").imagePath("Path image 2").build();
        imageMockList.add(imageMock2);

        when(imageProjectRepository.findAllImageByProjetcId(1))
                .thenReturn(ImageMapper.INSTANCE.toEntityList(imageMockList));

        final List<ImageDTO> imageDTOList = projectImageController.getAllImageForProject(1);
        Assertions.assertEquals(imageDTOList.size(), 2);
        Assertions.assertEquals(imageDTOList.get(0).getId(), 1);
        Assertions.assertEquals(imageDTOList.get(1).getId(), 2);
    }

    @Test
    void addImageForProject() {

        List<ImageDTO> imageMockDTOList = new ArrayList<>();
        Integer projectIdMock = 1;


        ImageDTO imageDTOMock = ImageDTO.builder().name("Name Mock").imagePath("path_mock").build();
        imageMockDTOList.add(imageDTOMock);
        ImageEntity imageEntityMock = ImageEntity.builder().id(1).name("Name Mock").imagePath("path_mock").build();

        when(imageRepository.findByImagePath(imageDTOMock.getImagePath())).thenReturn(imageEntityMock);
        when(imageProjectRepository.imageProjectExists(imageEntityMock.getId(), projectIdMock)).thenReturn(false);
        when(imageProjectRepository.joinImagesToTheProject(imageEntityMock.getId(), projectIdMock)).thenReturn(1);

        final List<ImageDTO> imageDTOList = projectImageController.addImageForProject(imageMockDTOList, projectIdMock);
        for (ImageDTO imageDTO : imageDTOList) {
            Assertions.assertEquals(imageDTO.getId(), 1);
        }

        when(imageRepository.findByImagePath(imageDTOMock.getImagePath())).thenReturn(null);
        when(imageProjectRepository.projectExists(projectIdMock)).thenReturn(true);
        when(imageProjectRepository.save(any())).thenReturn(imageEntityMock);
        when(imageProjectRepository.joinImagesToTheProject(imageEntityMock.getId(), projectIdMock)).thenReturn(1);

        final List<ImageDTO> imageDTOList1 = projectImageController.addImageForProject(imageMockDTOList, projectIdMock);
        for (ImageDTO imageDTO : imageDTOList1) {
            Assertions.assertEquals(imageDTO.getId(), 1);
        }
    }

    @Test
    void addImageForProjectException() {

        List<ImageDTO> imageMockDTOList = new ArrayList<>();
        Integer projectIdMock = 1;


        ImageDTO imageDTOMock = ImageDTO.builder().name("Name Mock").imagePath("path_mock").build();
        imageMockDTOList.add(imageDTOMock);
        ImageEntity imageEntityMock = ImageEntity.builder().id(1).name("Name Mock").imagePath("path_mock").build();

        when(imageRepository.findByImagePath(imageDTOMock.getImagePath())).thenReturn(imageEntityMock);
        when(imageProjectRepository.imageProjectExists(imageEntityMock.getId(), projectIdMock)).thenReturn(true);
        Assertions.assertThrows(ImageConflictException.class, () -> {
            projectImageController.addImageForProject(imageMockDTOList, projectIdMock);
        });

        when(imageRepository.findByImagePath(imageDTOMock.getImagePath())).thenReturn(imageEntityMock);
        when(imageProjectRepository.imageProjectExists(imageEntityMock.getId(), projectIdMock)).thenReturn(false);
        when(imageProjectRepository.joinImagesToTheProject(imageEntityMock.getId(), projectIdMock))
                .thenThrow(DataIntegrityViolationException.class);
        Assertions.assertThrows(ImageNotFoundException.class, () -> {
            projectImageController.addImageForProject(imageMockDTOList, projectIdMock);
        });

        when(imageRepository.findByImagePath(imageDTOMock.getImagePath())).thenReturn(null);
        when(imageProjectRepository.imageProjectExists(imageEntityMock.getId(), projectIdMock)).thenReturn(true);
        when(imageProjectRepository.projectExists(projectIdMock)).thenReturn(false);
        Assertions.assertThrows(ImageNotFoundException.class, () -> {
            projectImageController.addImageForProject(imageMockDTOList, projectIdMock);
        });
    }

    @Test
    void delImageByProject() {

        Integer imageIdMock = 1;
        Integer projectIdMock = 5;

        when(imageProjectRepository.deleteImageFroProject(imageIdMock, projectIdMock)).thenReturn(1);
        final String checkStatusSuccess = projectImageController.delImageByProject(imageIdMock, projectIdMock);
        Assertions.assertEquals(checkStatusSuccess, "Successful deletion of the image");

        when(imageProjectRepository.deleteImageFroProject(imageIdMock, projectIdMock)).thenReturn(0);
        final String checkStatusFailure = projectImageController.delImageByProject(imageIdMock, projectIdMock);
        Assertions.assertEquals(checkStatusFailure, "Failed deletion - image not found");
    }
}
