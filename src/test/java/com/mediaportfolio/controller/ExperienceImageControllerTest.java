package com.mediaportfolio.controller;

import com.mediaportfolio.exception.ImageConflictException;
import com.mediaportfolio.exception.ImageNotFoundException;
import com.mediaportfolio.model.ImageEntity;
import com.mediaportfolio.repository.ImageExperienceRepository;
import com.mediaportfolio.repository.ImageRepository;
import com.mediaportfolio.service.dto.ImageDTO;
import com.mediaportfolio.service.mapper.ImageMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataIntegrityViolationException;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class ExperienceImageControllerTest {

    @Mock
    ImageExperienceRepository imageExperienceRepository;

    @Mock
    ImageRepository imageRepository;

    @InjectMocks
    ExperienceImageController experienceImageController;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getAllImageByExperience() {

        List<ImageDTO> imageMockList = new ArrayList<>();

        ImageDTO imageMock = ImageDTO.builder().id(1).name("Nom image").imagePath("Path image").build();
        imageMockList.add(imageMock);
        ImageDTO imageMock2 = ImageDTO.builder().id(2).name("Nom image 2").imagePath("Path image 2").build();
        imageMockList.add(imageMock2);

        when(imageExperienceRepository.findAllImageByExperience(1))
                .thenReturn(ImageMapper.INSTANCE.toEntityList(imageMockList));

        final List<ImageDTO> imageDTOList = experienceImageController.getAllImageByExperience(1);
        Assertions.assertEquals(imageDTOList.size(), 2);
        Assertions.assertEquals(imageDTOList.get(0).getId(), 1);
        Assertions.assertEquals(imageDTOList.get(1).getId(), 2);
    }

    @Test
    void addImageForExperience() {

        List<ImageDTO> imageMockDTOList = new ArrayList<>();
        Integer experienceIdMock = 1;


        ImageDTO imageDTOMock = ImageDTO.builder().name("Name Mock").imagePath("path_mock").build();
        imageMockDTOList.add(imageDTOMock);
        ImageEntity imageEntityMock = ImageEntity.builder().id(1).name("Name Mock").imagePath("path_mock").build();

        when(imageRepository.findByImagePath(imageDTOMock.getImagePath())).thenReturn(imageEntityMock);
        when(imageExperienceRepository.imageExperienceExists(imageEntityMock.getId(), experienceIdMock)).thenReturn(false);
        when(imageExperienceRepository.joinImageToTheExperience(imageEntityMock.getId(), experienceIdMock)).thenReturn(1);

        final List<ImageDTO> imageDTOList = experienceImageController.addImageForExperience(imageMockDTOList, experienceIdMock);
        for (ImageDTO imageDTO : imageDTOList) {
            Assertions.assertEquals(imageDTO.getId(), 1);
        }

        when(imageRepository.findByImagePath(imageDTOMock.getImagePath())).thenReturn(null);
        when(imageExperienceRepository.experienceExists(experienceIdMock)).thenReturn(true);
        when(imageExperienceRepository.save(any())).thenReturn(imageEntityMock);
        when(imageExperienceRepository.joinImageToTheExperience(imageEntityMock.getId(), experienceIdMock)).thenReturn(1);

        final List<ImageDTO> imageDTOList1 = experienceImageController.addImageForExperience(imageMockDTOList, experienceIdMock);
        for (ImageDTO imageDTO : imageDTOList1) {
            Assertions.assertEquals(imageDTO.getId(), 1);
        }
    }

    @Test
    void addImageForExperienceException() {

        List<ImageDTO> imageMockDTOList = new ArrayList<>();
        Integer experienceIdMock = 1;


        ImageDTO imageDTOMock = ImageDTO.builder().name("Name Mock").imagePath("path_mock").build();
        imageMockDTOList.add(imageDTOMock);
        ImageEntity imageEntityMock = ImageEntity.builder().id(1).name("Name Mock").imagePath("path_mock").build();

        when(imageRepository.findByImagePath(imageDTOMock.getImagePath())).thenReturn(imageEntityMock);
        when(imageExperienceRepository.imageExperienceExists(imageEntityMock.getId(), experienceIdMock)).thenReturn(true);
        Assertions.assertThrows(ImageConflictException.class, () -> {
            experienceImageController.addImageForExperience(imageMockDTOList, experienceIdMock);
        });

        when(imageRepository.findByImagePath(imageDTOMock.getImagePath())).thenReturn(imageEntityMock);
        when(imageExperienceRepository.imageExperienceExists(imageEntityMock.getId(), experienceIdMock)).thenReturn(false);
        when(imageExperienceRepository.joinImageToTheExperience(imageEntityMock.getId(), experienceIdMock))
                .thenThrow(DataIntegrityViolationException.class);
        Assertions.assertThrows(ImageNotFoundException.class, () -> {
            experienceImageController.addImageForExperience(imageMockDTOList, experienceIdMock);
        });

        when(imageRepository.findByImagePath(imageDTOMock.getImagePath())).thenReturn(null);
        when(imageExperienceRepository.imageExperienceExists(imageEntityMock.getId(), experienceIdMock)).thenReturn(true);
        when(imageExperienceRepository.experienceExists(experienceIdMock)).thenReturn(false);
        Assertions.assertThrows(ImageNotFoundException.class, () -> {
            experienceImageController.addImageForExperience(imageMockDTOList, experienceIdMock);
        });
    }

    @Test
    void delImageByExperience() {

        Integer imageIdMock = 1;
        Integer experienceIdMock = 5;

        when(imageExperienceRepository.deleteImageByExperience(imageIdMock, experienceIdMock)).thenReturn(1);
        final String checkStatusSuccess = experienceImageController.delImageByExperience(imageIdMock, experienceIdMock);
        Assertions.assertEquals(checkStatusSuccess, "Successful deletion of the image");

        when(imageExperienceRepository.deleteImageByExperience(imageIdMock, experienceIdMock)).thenReturn(0);
        final String checkStatusFailure = experienceImageController.delImageByExperience(imageIdMock, experienceIdMock);
        Assertions.assertEquals(checkStatusFailure, "Failed deletion - image not found");
    }
}
