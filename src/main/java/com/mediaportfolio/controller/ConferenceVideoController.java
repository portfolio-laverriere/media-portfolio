package com.mediaportfolio.controller;

import com.mediaportfolio.exception.VideoConflictException;
import com.mediaportfolio.exception.VideoNotFoundException;
import com.mediaportfolio.repository.VideoConferenceRepository;
import com.mediaportfolio.service.dto.VideoDTO;
import com.mediaportfolio.service.mapper.VideoMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Api( description = "API POUR LES OPERATIONS CRUD SUR LES VIDEOS / CONFERENCE" )
@RestController
public class ConferenceVideoController {

    private static final Logger logger = LoggerFactory.getLogger( ConferenceVideoController.class );

    @Autowired
    private VideoConferenceRepository videoConferenceRepository;


                                        /* ===================================== */
                                        /* ================ GET ================ */
                                        /* ===================================== */

    /* --------------------------------------- GET VIDEO BY CONFERENCE ---------------------------------------- */
    @ApiOperation( value = "GET ALL VIDEOS BY CONFERENCE" )
    @GetMapping( value = "/media/video/conference/get-all-video/{conferenceId}" )
    public List<VideoDTO> getVideoByConference(@PathVariable Integer conferenceId) {

        List<VideoDTO> videoDTOList = new ArrayList<>();

        try {
            /**
             * OBTENIR TOUTES LES VIDEOS PAR L'ID CONFERENCE
             */
            videoDTOList = VideoMapper.INSTANCE.toDTOList(videoConferenceRepository.findAllByConferenceId(conferenceId));
        } catch (Exception pEX) {
            logger.error("{}", String.valueOf(pEX));
        }
        return videoDTOList;
    }


                                        /* ===================================== */
                                        /* ================ ADD ================ */
                                        /* ===================================== */

    /* --------------------------------------- ADD VIDEO FOR CONFERENCE --------------------------------------- */
    @ApiOperation(value = "ADD VIDEO FOR CONFERENCE")
    @PostMapping(value = "/media/video/conference/add-video/{conferenceId}")
    public List<VideoDTO> addVideoForConference(@RequestBody List<VideoDTO> videoDTOList,
                                                @PathVariable Integer conferenceId) {

        for (VideoDTO videoDTO : videoDTOList) {
            /** J'AJOUTE A VIDEO L'ID DE CONFERENCE AUQUEL CELUI-CI EST LIE */
            videoDTO.setConferenceId(conferenceId);

            Boolean videoExists = videoConferenceRepository.videoExistsByUrl(videoDTO.getVideoUrl());

            if (!videoExists) {

                /** J'AJOUTE LA VIDEO, PUIS JE RECUPERE SON IDENTIFIANT */
                videoDTO.setId(VideoMapper.INSTANCE.toDTO(
                        videoConferenceRepository.save(VideoMapper.INSTANCE.toEntity(videoDTO))).getId());

            } else {
                throw new VideoConflictException("The video for the following url '" + videoDTO.getVideoUrl() +
                        "' has already been added");
            }
        }
        return videoDTOList;
    }


                                        /* ===================================== */
                                        /* ============== UPDATE =============== */
                                        /* ===================================== */

    /* ------------------------------------------ UP VIDEO ------------------------------------------ */
    @ApiOperation(value = "UP VIDEO BY CONFERENCE")
    @PutMapping(value = "/media/video/conference/up-video")
    public VideoDTO upVideoByConference(@RequestBody VideoDTO videoDTO) {

        /**
         * JE VERIFIE SI LA VIDEO EXISTE
         */
        Boolean videoExists = videoConferenceRepository.existsById(videoDTO.getId());

        if(videoExists) {

            try {
                videoDTO = VideoMapper.INSTANCE.toDTO(videoConferenceRepository.save(VideoMapper.INSTANCE.toEntity(videoDTO)));
            } catch (Exception pEX) {
                logger.error("{}", String.valueOf(pEX));
            }

        } else {
            logger.error("Update failure - video : '" + videoDTO.getTitle() + "' - Is not found");
            throw new VideoNotFoundException("Update failure - video : '" + videoDTO.getTitle() + "' - Is not found");
        }
        return videoDTO;
    }


                                    /* ======================================= */
                                    /* =============== DELETE  =============== */
                                    /* ======================================= */

    /* --------------------------------------- DEL VIDEO BY CONFERENCE --------------------------------------- */
    @ApiOperation(value = "DEL VIDEO")
    @DeleteMapping(value = "/media/video/conference/del-video/{videoId}")
    public String delVideoByConference(@PathVariable Integer videoId) {

        String massageStatus = "";

        try {
            /** @see VideoConferenceRepository#deleteVideoById(Integer)  */
            Integer check = videoConferenceRepository.deleteVideoById(videoId);

            if (check == 0) {
                massageStatus = "Failed deletion - video not found";
            } else {
                massageStatus = "Successful deletion of the video";
            }
        } catch (Exception pEX) {
            logger.error(String.valueOf(pEX));
        }
        return massageStatus;
    }
}
