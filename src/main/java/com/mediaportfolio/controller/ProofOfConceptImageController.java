package com.mediaportfolio.controller;

import com.mediaportfolio.exception.ImageConflictException;
import com.mediaportfolio.exception.ImageNotFoundException;
import com.mediaportfolio.model.ImageEntity;
import com.mediaportfolio.repository.ImageProofOfConceptRepository;
import com.mediaportfolio.repository.ImageRepository;
import com.mediaportfolio.service.dto.ImageDTO;
import com.mediaportfolio.service.mapper.ImageMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@Api( description = "API POUR LES OPERATIONS CRUD SUR LES IMAGES / PROOF OF CONCEPT" )
@RestController
public class ProofOfConceptImageController {

    private static final Logger logger = LoggerFactory.getLogger(ProofOfConceptImageController.class);

    @Autowired
    private ImageProofOfConceptRepository imageProofOfConceptRepository;

    @Autowired
    private ImageRepository imageRepository;


                                        /* ===================================== */
                                        /* ================ GET ================ */
                                        /* ===================================== */

    /* --------------------------------- GET ALL IMAGES BY PROOF OF CONCEPT ------------------------------------- */
    @ApiOperation(value = "GET ALL IMAGES BY PROOF OF CONCEPT")
    @GetMapping(value = "/media/poc/get-all-image/{pocId}")
    public List<ImageDTO> getAllImageByProofOfConcept(@PathVariable Integer pocId) {

        List<ImageDTO> imageDTOList = new ArrayList<>();

        try {
            /**
             * OBTENIR TOUTES LES IMAGES PAR L'ID PROOF OF CONCEPT
             * @see ImageProofOfConceptRepository#findAllImageByProofOfConceptId(Integer)
             */
            imageDTOList = ImageMapper.INSTANCE.toDTOList(imageProofOfConceptRepository.findAllImageByProofOfConceptId(pocId));
        } catch (Exception pEX) {
            logger.error("{}", String.valueOf(pEX));
        }
        return imageDTOList;
    }

                                        /* ===================================== */
                                        /* ================ ADD ================ */
                                        /* ===================================== */

    /* ----------------------------------- ADD IMAGE FOR PROOF OF CONCEPT ----------------------------------- */
    @ApiOperation(value = "ADD IMAGE FOR PROOF OF CONCEPT")
    @PostMapping(value = "/media/poc/add-image/{pocId}")
    public List<ImageDTO> addImageForProofOfConcept(@RequestBody List<ImageDTO> imageDTOList,
                                             @PathVariable Integer pocId) {

        for (ImageDTO imageDTO : imageDTOList) {

            /** JE VERIFIE SI L'IMAGE EST DEJA PRESENTE DANS LA TABLE
             * @see ImageRepository#findByImagePath(String)
             */
            ImageEntity imageEntity = imageRepository.findByImagePath(imageDTO.getImagePath());

            /**
             * SI L'IMAGE EST DEJA PRESENTE DANS LA TABLE, JE JOINS CELLE-CI A LA TABLE PROOF OF CONCEPT DANS
             * LA TABLE DE JOINTURE "IMAGE_PROOFOFCONCEPT", SINON J'AJOUTE LA NOUVELLE IMAGE ET JE CREE LA JOINTURE
             */
            if (imageEntity != null) {

                /** JE VERIFIE SI LE PROOF OF CONCEPT ET L'IMAGE SON DEJA LIES
                 * @see ImageProofOfConceptRepository#imageProofOfConceptExists(Integer, Integer)
                 */
                Boolean imageProofOfConceptExists = imageProofOfConceptRepository
                        .imageProofOfConceptExists(imageEntity.getId(), pocId);

                if (!imageProofOfConceptExists) {

                    try {

                        /** @see ImageProofOfConceptRepository#joinImagesToTheProofOfConcept(Integer, Integer) */
                        imageProofOfConceptRepository.joinImagesToTheProofOfConcept(imageEntity.getId(), pocId);
                        imageDTO.setId(imageEntity.getId());

                    } catch (DataIntegrityViolationException pEX) {
                        throw new ImageNotFoundException("Proof of concept not found for the ID : " + pocId +
                                " - Impossible to link the image to the proof of concept");
                    } catch (Exception pEX) {
                        logger.error(String.valueOf(pEX));
                    }

                } else {
                    throw new ImageConflictException("The image is already linked to the proof of concept: " + pocId);
                }

            } else {
                /** JE VERIFIE SI LE PROOF OF CONCEPT EXISTE
                 * @see ImageProofOfConceptRepository#proofOfConceptExists(Integer)
                 */
                Boolean proofOfConceptExists = imageProofOfConceptRepository.proofOfConceptExists(pocId);

                if (proofOfConceptExists) {
                    /** J'AJOUTE L'IMAGE, PUIS JE RECUPERE SON IDENTIFIANT */
                    imageDTO.setId(ImageMapper.INSTANCE.toDTO(
                            imageProofOfConceptRepository.save(ImageMapper.INSTANCE.toEntity(imageDTO))).getId());

                    /** @see ImageProofOfConceptRepository#joinImagesToTheProofOfConcept(Integer, Integer) */
                    imageProofOfConceptRepository.joinImagesToTheProofOfConcept(imageDTO.getId(), pocId);
                } else {
                    throw new ImageNotFoundException("Proof of concept not found for the ID : " + pocId +
                            " - Impossible to add the image and link it to the proof of concept");
                }
            }
        }
        return imageDTOList;
    }


                                        /* ======================================= */
                                        /* =============== DELETE  =============== */
                                        /* ======================================= */

    /* ------------------------------------- DEL IMAGE BY PROOF OF CONCEPT ------------------------------------- */
    @ApiOperation(value = "DEL IMAGE BY PROOF OF CONCEPT")
    @DeleteMapping(value = "/media/poc/del-image/{imageId}/{pocId}")
    public String delImageByProofOfConcept(@PathVariable Integer imageId, @PathVariable Integer pocId) {

        String massageStatus = "";

        try {
        /** @see ImageProofOfConceptRepository#deleteImageFroProofOfConcept(Integer, Integer) */
            Integer check = imageProofOfConceptRepository.deleteImageFroProofOfConcept(imageId, pocId);

            if (check == 0) {
                massageStatus = "Failed deletion - image not found";
            } else {
                massageStatus = "Successful deletion of the image";
            }

        } catch (Exception pEX) {
            logger.error(String.valueOf(pEX));
        }
        return massageStatus;
    }
}
