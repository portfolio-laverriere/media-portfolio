package com.mediaportfolio.controller;

import com.mediaportfolio.exception.ImageConflictException;
import com.mediaportfolio.exception.ImageNotFoundException;
import com.mediaportfolio.model.ImageEntity;
import com.mediaportfolio.repository.ImageDiplomaRepository;
import com.mediaportfolio.repository.ImageRepository;
import com.mediaportfolio.service.dto.ImageDTO;
import com.mediaportfolio.service.mapper.ImageMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@Api( description = "API POUR LES OPERATIONS CRUD SUR LES IMAGES / DIPLOMA" )
@RestController
public class DiplomaImageController {

    private static final Logger logger = LoggerFactory.getLogger( DiplomaImageController.class );

    @Autowired
    private ImageDiplomaRepository imageDiplomaRepository;
    
    @Autowired
    private ImageRepository imageRepository;


                                        /* ===================================== */
                                        /* ================ GET ================ */
                                        /* ===================================== */

    /* --------------------------------------- GET IMAGES BY DIPLOMA ---------------------------------------- */
    @ApiOperation(value = "GET ALL IMAGES BY DIPLOMA")
    @GetMapping(value = "/media/image/diploma/get-all-image/{diplomaId}")
    public List<ImageDTO> getAllImageByDiploma(@PathVariable Integer diplomaId) {

        List<ImageDTO> imageDTOList = new ArrayList<>();

        try {
            /**
             *  OBTENIR TOUTES LES IMAGES PAR L'ID DIPLOMA
             * @see ImageDiplomaRepository#findAllImageByDiplomaId(Integer)
             */
            imageDTOList = ImageMapper.INSTANCE.toDTOList(imageDiplomaRepository.findAllImageByDiplomaId(diplomaId));
        } catch (Exception pEX) {
            logger.error("{}", String.valueOf(pEX));
        }
        return imageDTOList;
    }


                                        /* ===================================== */
                                        /* ================ ADD ================ */
                                        /* ===================================== */

    /* --------------------------------------- ADD IMAGE FOR DIPLOMA --------------------------------------- */
    @ApiOperation(value = "ADD IMAGE FOR DIPLOMA")
    @PostMapping(value = "/media/image/diploma/add-image/{diplomaId}")
    public List<ImageDTO> addImageForDiploma(@RequestBody List<ImageDTO> imageDTOList, 
                                             @PathVariable Integer diplomaId) {
        
        for (ImageDTO imageDTO : imageDTOList) {
            /**
             * JE VERIFIE SI L'IMAGE EST DEJA PRESENTE DANS LA TABLE
             * @see ImageRepository#findByImagePath(String)
             */
            ImageEntity imageEntity = imageRepository.findByImagePath(imageDTO.getImagePath());

            /**
             * SI L'IMAGE EST DEJA PRESENTE DANS LA TABLE, JE JOINS CELLE-CI A LA TABLE DIPLOMA DANS
             * LA TABLE DE JOINTURE "IMAGE_DIPLOMA", SINON J'AJOUTE LA NOUVELLE IMAGE ET JE CREE LA JOINTURE
             */
            if(imageEntity != null) {

                /** JE VERIFIE SI DIPLOMA ET L'IMAGE SON DEJA LIES
                 * @see ImageDiplomaRepository#imageDiplomaExists(Integer, Integer)
                 */
                Boolean imageDiplomaExists = imageDiplomaRepository.imageDiplomaExists(imageEntity.getId(), diplomaId);

                if (!imageDiplomaExists) {

                    try {

                        /** @see ImageDiplomaRepository#joinImageToTheDiploma(Integer, Integer) */
                        imageDiplomaRepository.joinImageToTheDiploma(imageEntity.getId(), diplomaId);
                        imageDTO.setId(imageEntity.getId());

                    } catch (DataIntegrityViolationException pEX) {
                        throw new ImageNotFoundException("Diploma not found for the ID : " + diplomaId +
                                " - Impossible to link the image to the diploma");
                    } catch (Exception pEX) {
                        logger.error(String.valueOf(pEX));
                    }

                } else {
                    throw new ImageConflictException("The image is already linked to the diploma : " + diplomaId);
                }

            } else {
                /** JE VERIFIE SI LE DIPLOMA EXISTE
                 * @see ImageDiplomaRepository#diplomaExists(Integer)
                 */
                Boolean diplomaExists = imageDiplomaRepository.diplomaExists(diplomaId);

                if (diplomaExists) {

                    /** J'AJOUTE L'IMAGE, PUIS JE RECUPERE SON IDENTIFIANT */
                    imageDTO.setId(ImageMapper.INSTANCE.toDTO(
                            imageDiplomaRepository.save(ImageMapper.INSTANCE.toEntity(imageDTO))).getId());

                    /** @see ImageDiplomaRepository#joinImageToTheDiploma(Integer, Integer) */
                    imageDiplomaRepository.joinImageToTheDiploma(imageDTO.getId(), diplomaId);

                } else {
                    throw new ImageNotFoundException("Diploma not found for the ID : " + diplomaId +
                            " - Impossible to add the image and link it to the diploma");
                }
            }
        }
        return imageDTOList;
    }


                                    /* ======================================= */
                                    /* =============== DELETE  =============== */
                                    /* ======================================= */

    /* --------------------------------------- DEL IMAGE BY DIPLOMA --------------------------------------- */
    @ApiOperation(value = "DEL IMAGE BY DIPLOMA")
    @DeleteMapping(value = "/media/image/diploma/del-image/{imageId}/{diplomaId}")
    public String delImageByDiploma(@PathVariable Integer imageId, @PathVariable Integer diplomaId) {

        String massageStatus = "";

        try {
            /** @see ImageDiplomaRepository#deleteImageByDiploma(Integer, Integer) */
            Integer check = imageDiplomaRepository.deleteImageByDiploma(imageId, diplomaId);

            if (check == 0) {
                massageStatus = "Failed deletion - image not found";
            } else {
                massageStatus = "Successful deletion of the image";
            }

        } catch (Exception pEX) {
            logger.error(String.valueOf(pEX));
        }
        return massageStatus;
    }

}
