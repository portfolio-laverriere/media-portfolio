package com.mediaportfolio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class ImageConflictException extends RuntimeException {

    public ImageConflictException(String s) {
        super(s);
    }
}
