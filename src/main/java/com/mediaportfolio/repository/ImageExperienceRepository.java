package com.mediaportfolio.repository;

import com.mediaportfolio.model.ImageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository
public interface ImageExperienceRepository extends JpaRepository<ImageEntity, Integer> {


    /**
     * OBTENIR TOUTES LES IMAGES PAR L'ID DE EXPERIENCE
     * @param experienceId
     * @return
     */
    @Query(value = "SELECT DISTINCT image.* FROM image" +
            " INNER  JOIN image_experience ON image_experience.experience_id= :experienceId" +
            " WHERE image.id= image_experience.image_id", nativeQuery = true)
    List<ImageEntity> findAllImageByExperience(@Param("experienceId") Integer experienceId);


    /**
     * VERIFIE SI LE LIEN ENTRE LA TABLE IMAGE ET EXPERIENCE EXISTE DEJA
     * @param imageId
     * @param experienceId
     * @return
     */
    @Query(value = "SELECT count(image_experience) > 0 FROM image_experience" +
            " WHERE image_experience.image_id= :imageId AND image_experience.experience_id= :experienceId", nativeQuery = true)
    Boolean imageExperienceExists(@Param("imageId") Integer imageId, @Param("experienceId") Integer experienceId);


    /**
     * LIER DANS LA TABLE DE JOINTURE IMAGE ET EXPERIENCE
     * @param imageId
     * @param experienceId
     */
    @Modifying @Transactional
    @Query(value = "INSERT INTO image_experience (image_id, experience_id)" +
            " VALUES (:imageId, :experienceId)", nativeQuery = true)
    int joinImageToTheExperience(@Param("imageId") Integer imageId, @Param("experienceId") Integer experienceId);


    /**
     * SUPPRIMER LA JOINTURE ENTRE IMAGE ET EXPERIENCE
     * @param imageId
     * @param experienceId
     */
    @Modifying @Transactional
    @Query(value = "DELETE FROM image_experience" +
            " WHERE image_id= :imageId AND experience_id= :experienceId", nativeQuery = true)
    int deleteImageByExperience(@Param("imageId") Integer imageId, @Param("experienceId") Integer experienceId);


    /**
     * VERIFIE SI EXPERIENCE EXISTE
     * @param experienceId
     * @return
     */
    @Query(value = "SELECT COUNT(experience) > 0 FROM experience WHERE experience.id= :experienceId", nativeQuery = true)
    Boolean experienceExists(@Param("experienceId") Integer experienceId);

}
