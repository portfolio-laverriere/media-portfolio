package com.mediaportfolio.repository;

import com.mediaportfolio.model.ImageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface ImageRepository extends JpaRepository<ImageEntity, Integer> {


    /**
     *  OBTENIR L'IMAGE PAR SON PATH
     * @param imagePath
     * @return
     */
    ImageEntity findByImagePath(String imagePath);


    /**
     * VERIFIE SI L'IMAGE ET UTILISER DANS LA TABLE PROJECT
     * @param imageId
     * @return
     */
    @Query(value = "SELECT DISTINCT name FROM project " +
            " INNER JOIN image_project ON image_project.image_id= :imageId" +
            " WHERE project.id= image_project.project_id", nativeQuery = true)
    String imageUseInTheTableProject(@Param("imageId") Integer imageId);


    /**
     * VERIFIE SI L'IMAGE ET UTILISER DANS LA TABLE PROOF OF CONCEPT
     * @param imageId
     * @return
     */
    @Query(value = "SELECT DISTINCT name FROM proof_of_concept" +
            " INNER JOIN image_proofofconcept ON image_proofofconcept.image_id= :imageId" +
            " WHERE proof_of_concept.id= image_proofofconcept.proof_of_concept_id", nativeQuery = true)
    String imageUseInTheTableProfOfConcept(@Param("imageId") Integer imageId);


    /**
     * VERIFIE SI L'IMAGE ET UTILISER DANS LA TABLE PARAGRAPH ET RENVOIE LE TITRE DE L'ARTICLE AUQUEL IL APPARTIENT
     * @param imageId
     * @return
     */
    @Query(value = "SELECT DISTINCT title FROM article" +
            " INNER JOIN image_paragraph ON image_paragraph.image_id= :imageId" +
            " INNER JOIN paragraph ON paragraph.id= image_paragraph.paragraph_id" +
            " WHERE article.id= paragraph.article_id", nativeQuery = true)
    String imageUseInTheTableParagraph(@Param("imageId") Integer imageId);


    /**
     * VERIFIE SI L'IMAGE ET UTILISER DANS LA TABLE EXPERIENCE
     * @param imageId
     * @return
     */
    @Query(value = "SELECT DISTINCT title FROM experience" +
            " INNER JOIN image_experience ON image_experience.image_id= :imageId" +
            " WHERE image_experience.id= image_experience.experience_id", nativeQuery = true)
    String imageUseInTheTableExperience(@Param("imageId") Integer imageId);


    /**
     * VERIFIE SI L'IMAGE ET UTILISER DANS LA TABLE DIPLOMA
     * @param imageId
     * @return
     */
    @Query(value = "SELECT DISTINCT title FROM diploma" +
            " INNER JOIN image_diploma ON image_diploma.image_id= :imageId" +
            " WHERE diploma.id= image_diploma.diploma_id", nativeQuery = true)
    String imageUseInTheTableDiploma(@Param("imageId") Integer imageId);


    /**
     * VERIFIE SI L'IMAGE ET UTILISER DANS LA TABLE CONFERENCE
     * @param imageId
     * @return
     */
    @Query(value = "SELECT DISTINCT name FROM conference" +
            " INNER JOIN image_conference ON image_conference.image_id= :imageId" +
            " WHERE conference.id= image_conference.conference_id", nativeQuery = true)
    String imageUseInTheTableConference(@Param("imageId") Integer imageId);

    @Modifying @Transactional
    @Query(value = "DELETE FROM image" +
            " WHERE image.id= :imageId", nativeQuery = true)
    int deleteImage(@Param("imageId") Integer imageId);

}
