package com.mediaportfolio.repository;

import com.mediaportfolio.model.ImageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository
public interface ImageParagraphRepository extends JpaRepository<ImageEntity, Integer> {


    /**
     * OBTENIR TOUTES LES IMAGES PAR L'ID DU PARAGRAPH
     * @param paragraphId
     * @return
     */
    @Query(value = "SELECT DISTINCT image.* FROM image" +
            " INNER JOIN image_paragraph ON image_paragraph.paragraph_id= :paragraphId" +
            " WHERE image.id= image_paragraph.image_id", nativeQuery = true)
    List<ImageEntity> findAllImageByParagraphId(@Param("paragraphId") Integer paragraphId);


    /**
     * VERIFIE SI LE LIEN ENTRE LA TABLE IMAGE ET PARAGRAPH EXISTE DEJA
     * @param imageId
     * @param paragraphId
     * @return
     */
    @Query(value = "SELECT count(image_paragraph) > 0 FROM image_paragraph" +
            " WHERE image_paragraph.image_id= :imageId AND image_paragraph.paragraph_id= :paragraphId", nativeQuery = true)
    Boolean imageParagraphExists(@Param("imageId") Integer imageId, @Param("paragraphId") Integer paragraphId);

    /**
     * LIER DANS LA TABLE DE JOINTURE L'IMAGE AU PARAGRAPH
     * @param imageId
     * @param paragraphId
     */
    @Modifying @Transactional
    @Query(value = "INSERT INTO image_paragraph (image_id, paragraph_id)" +
            " VALUES (:imageId, :paragraphId)", nativeQuery = true)
    int joinImageToTheParagraph(@Param("imageId") Integer imageId, @Param("paragraphId") Integer paragraphId);


    /**
     * SUPPRIMER LA JOINTURE ENTRE UNE IMAGE ET UN PARAGRAPH
     * @param imageId
     * @param paragraphId
     */
    @Modifying @Transactional
    @Query(value = "DELETE FROM image_paragraph" +
            " WHERE image_id= :imageId AND paragraph_id= :paragraphId", nativeQuery = true)
    int deleteImageByParagraph(@Param("imageId") Integer imageId, @Param("paragraphId") Integer paragraphId);


    /**
     * VERIFIE SI LE PARAGRAPH EXISTE
     * @param paragraphId
     * @return
     */
    @Query(value = "SELECT COUNT(paragraph) > 0 FROM paragraph WHERE paragraph.id= :paragraphId", nativeQuery = true)
    Boolean paragraphExists(@Param("paragraphId") Integer paragraphId);

}
